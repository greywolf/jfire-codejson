package link.jfire.codejson.function;

import link.jfire.baseutil.collection.StringCache;

public abstract class WriterAdapter implements JsonWriter
{
    
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(int field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(float field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(double target, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(long field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(byte field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(char field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(short field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
    @Override
    public void write(boolean field, StringCache cache, Object entity)
    {
        throw new RuntimeException("没有实现");
    }
    
}
