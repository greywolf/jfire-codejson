package link.jfire.codejson.function.impl.write.array;

import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;

public class FloatArrayWriter extends WriterAdapter
{
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        float[] array = (float[]) field;
        cache.append('[');
        for (float each : array)
        {
            cache.append(each).append(',');
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append(']');
    }
}
