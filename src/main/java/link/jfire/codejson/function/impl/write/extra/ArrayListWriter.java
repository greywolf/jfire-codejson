package link.jfire.codejson.function.impl.write.extra;

import java.util.ArrayList;
import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;
import link.jfire.codejson.function.WriterContext;

public class ArrayListWriter extends WriterAdapter
{
    
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        ArrayList<?> list = (ArrayList<?>) field;
        cache.append('[');
        int size = list.size();
        Object value = null;
        for (int i = 0; i < size; i++)
        {
            if ((value = list.get(i)) != null)
            {
                if (value instanceof String)
                {
                    cache.append('"').append((String) value).append('"');
                }
                else
                {
                    WriterContext.write(value, cache);
                    
                }
                cache.append(',');
            }
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append(']');
    }
    
}
