package link.jfire.codejson.function.impl.write.wrapper;

import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;

public class StringWriter extends WriterAdapter implements WrapperWriter
{
    
    public void write(Object field, StringCache cache, Object entity)
    {
        cache.append('\"').append((String) field).append('\"');
    }
    
}
