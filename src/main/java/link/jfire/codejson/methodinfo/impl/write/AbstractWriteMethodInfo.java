package link.jfire.codejson.methodinfo.impl.write;

import java.lang.reflect.Method;
import link.jfire.codejson.methodinfo.WriteMethodInfo;
import link.jfire.codejson.strategy.WriteStrategy;

public abstract class AbstractWriteMethodInfo implements WriteMethodInfo
{
    protected String        str;
    protected String        getValue;
    protected WriteStrategy strategy;
    protected String        entityName;
    
    public AbstractWriteMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        this.entityName = entityName;
        this.strategy = strategy;
        getValue = entityName + "." + method.getName() + "()";
    }
    
    public String getOutput()
    {
        return str;
    }
}
